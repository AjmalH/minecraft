-- Note : This program is in Beta. Please DONNOT distribute without my permission. Thanks.
-- Ce programme est en Beta. Merci de ne pas le distribuer sans ma permission.
-- RootSlayers.

Version = "28/07/14 - V2"


-- GLOBAL VARIABLE --

PasbinURLID = "Ydy5DLsX"
URL_Date = "http://192.168.1.3/_API/Date.php"
URL_Time = "http://192.168.1.3/_API/Time.php"

-- GLOBAL VARIABLE --


local tArgs = {...}
DirInstall = shell.getRunningProgram()
FolInstall = fs.getDir(shell.getRunningProgram())
local oldShutdown = os.shutdown
local oldReboot = os.reboot

local function searchArgs(text)
	for k,v in pairs(tArgs) do
		if v == text then return true end
	end
	return false
end

-- Update part --
local function update(url)
    http.request(url)
	while true do
		local e, rUrl, rmsg = os.pullEvent()
		if (e == "http_success") and (rUrl == url) then
			if rmsg then
				local data = rmsg.readAll()
				rmsg.close()
				if data then
					return "success", data
				else
					sleep(1)
					http.request(url)
				end
			else
				sleep(1)
				http.request(url)
			end
		elseif (e == "http_failure") and (rUrl == url) then
			return "failure"
		end
	end
end
function commandUpdate(removeSettings)
	print("Updating...")
	size = 0
	-- local resp, data = update("http://pastebin.com/raw.php?i=VinNhvuD")
    local resp, data = update("http://pastebin.com/raw.php?i="..PasbinURLID)
    if resp == "success" then
		local f = io.open(DirInstall, "w")
		f:write(data)
		f:close()
		size = fs.getSize(DirInstall)
        
        if removeSettings then
            fs.delete(FolInstall.."/Settings")
            print("Removing "..FolInstall.."/Settings")
        end
        
		print("Updated. ("..size.." bytes)")
        
        sleep(1)
        os.reboot()
	elseif resp == "failure" then
		print("Failed.")
	end
end
if searchArgs("update") then
    if searchArgs("removeSettings") then
        commandUpdate(true)
    else
        commandUpdate(false)
    end    
end
-- Update part --

-- Settings edition
function EditSettings()
  	term.clear()
    term.setCursorPos(1, 1)
    term.setTextColor(colors.blue)
    print("Settings :")
    print("")
    --term.setTextColor(colors.white)
    print("[>] Date_Format  : ", ProgSettings["Date_Format"])
    print("[>] Modem        : ", ProgSettings["Modem_1"])
    print("[>] Time_Format  : ", ProgSettings["Time_Format"])
    print("[>] Bridge       : ", ProgSettings["Bridge"])
    print("[>] English      : ", ProgSettings["English"])
    print("[>] TimeOf       : ", ProgSettings["getTimeOF"])
    print("[>] Debug        : ", ProgSettings["Debug"])
    print("[>] GTM +        : ", ProgSettings["GTM"])
    print("[>] Save & Reboot")
    print("[>] Back without saving")
    
    while 29 do
        local event, button, x, y = os.pullEvent("mouse_click")
        
        if (x == 1) or (x==2) or (x==3) then
            if y == 3 then
                local Input2 = read()
                ProgSettings["Date_Format"] = Input2
                EditSettings()
            elseif y == 4 then
                local Input2 = read()
                ProgSettings["Modem_1"] = Input2
                EditSettings()
            elseif y == 5 then
                if ProgSettings["Time_Format"] == "12H" then
                    ProgSettings["Time_Format"] = "24H"
                    EditSettings()
                else
                    ProgSettings["Time_Format"] = "12H"
                    EditSettings()
                end  
            elseif y == 6 then
                local Input2 = read()
                ProgSettings["Bridge"] = Input2
                EditSettings()
            elseif y == 7 then
                if ProgSettings["English"] then
                    ProgSettings["English"] = false
                    EditSettings()
                else
                    ProgSettings["English"] = true
                    EditSettings()
                end    
            elseif y == 8 then
                if ProgSettings["getTimeOF"] == "IG" then
                    ProgSettings["getTimeOF"] = "IRL"
                    EditSettings()
                else
                    ProgSettings["getTimeOF"] = "IG"
                    EditSettings()
                end  
            elseif y == 9 then
                if ProgSettings["Debug"] then
                    ProgSettings["Debug"] = false
                    EditSettings()
                else
                    ProgSettings["Debug"] = true
                    EditSettings()
                end  
            elseif y == 10 then
                local Input2 = read()
                ProgSettings["GTM"] = Input2
                EditSettings()
            elseif y == 11 then
                print("Saved.")
                WriteFile()
                os.reboot()
            elseif y == 12 then
                MainScreen()        
            else
                EditSettings()
            end
        end
    end        
end
-- Settings edition

-- TabCompletion --
local parameters = { ["update "] = {["removeSettings"] = {}}, ["nodaemon"] = {}, ["update"] = {}}
local function tabCompletionFunction(shell, parNumber, curText, lastText)
	-- Check that the parameters entered so far are valid:
	local curParam = parameters
	for i = 2, #lastText do
		if curParam[lastText[i] .. " "] then
			curParam = curParam[lastText[i] .. " "]
		else
			return {}
		end
	end

	-- Check for suitable words for the current parameter:
	local results = {}
	for word, _ in pairs(curParam) do
		if word:sub(1, #curText) == curText then
			results[#results + 1] = word:sub(#curText + 1)
		end
	end
	return results
end

shell.setCompletionFunction(DirInstall, tabCompletionFunction)
-- TabCompletion --


 
function os.shutdown()              -- Permet d'avoir l'animation avant un shutdown.                            -- Animation before a shutdown.
    closeAll()
    Shutdown = true
    sleep(2)
    return oldShutdown()
end
 
function os.reboot()                -- Permet d'avoir l'animation avant un reboot.                              -- Animation before a reboot.
    closeAll()
    Shutdown = true
    sleep(2)
    return oldReboot()
end

function Void()                     -- Fonctions vide.                                                          -- Empty function.
    -- Cette fonction ne sert a rien.
end

function WriteFile()                -- ...                                                                      -- ...
    local file = io.open(FolInstall.."/Settings", "w")
    file:write(textutils.serialize(ProgSettings))
    file:close()
end

function ReadFile()                 -- Cette fonction permet de lire le fichier de Configuration : /Settings.   --  Read /Settings file if exists.

    if fs.exists(FolInstall.."/Settings") and not(fs.isDir(FolInstall.."/Settings")) then   -- Lecture du fichier si il existe.         --  Read /Settings file if exists.
        local file = io.open(FolInstall.."/Settings", "r")
        local data = file:read("*a")
        ProgSettings = textutils.unserialize(data)
        file:close()
    else
        term.setTextColor(colors.blue)
        print("Leave empty if not connected.")
        
        term.setTextColor(colors.red)
        print("Peripheral Bridge CANNOT be empty.")
        term.setTextColor(colors.white)    
         
        -- Settings File :
        ProgSettings = {
            Date_Format = "N/D",
            Time_Format = "N/D",
            getTimeOF = "N/D", 
            GTM = "2",
        }
        
        print("")
        print("Peripheral Bridge :")
        local B = read()
        if B ~= "" then
            ProgSettings["Bridge"] = B
        else
            print("This program need a Terminal Glasses Bridge to work !")
            ReadFile()
        end
            
        
        -- print("")
        -- print("IOPeripheral :")
        -- local P = read()
        -- if P ~= "" then
            -- ProgSettings["IOPeripheral"] = P
        -- end
        
        
        print("")
        print("Modem :")
        local M = read()
        if M ~= "" then
            ProgSettings["Modem_1"] = M
        else
            ProgSettings["Modem_1"] = false
        end
        
        --
        print("")
        print("Debug Mode :     Y-N")                   -- Mettre ou non en Debug.
        local D = read()
        if (D ~= "" and D ~= "n" and D ~= "N") and (D == "Y" or D == "y") then
            ProgSettings["Debug"] = true
        else
            ProgSettings["Debug"] = false
        end
        
        --
        print("")
        print("Francais ?        Y-N")
        local F = read()
        if (F ~= "" and F ~= "n" and F ~= "N") and (F == "Y" or F == "y") then
            ProgSettings["English"] = false
        else
            ProgSettings["English"] = true
        end
       
        --
        print("")    
        WriteFile()
        term.clear()
        term.setCursorPos(1, 1)              
    end
end

function getURL(url)                -- Requete HTTP.                                                            -- HTTP request.
	http.request(url)
	while true do
		local e, rUrl, rmsg = os.pullEvent()
		if (e == "http_success") and (rUrl == url) then
			if rmsg then
				local data = rmsg.readAll()
				rmsg.close()
				if data then
					return "success", data
				else
					sleep(1)
					http.request(url)
				end
			else
				sleep(1)
				http.request(url)
			end
		elseif (e == "http_failure") and (rUrl == url) then
			return "failure"
		end
	end
end

function LinkPeriph()               -- 'Attachement' des peripheriques.                                         -- Setup linked peripherals.
        if ProgSettings["Modem_1"] then
        if peripheral.getType(ProgSettings["Modem_1"]) == "modem" then
            rednet.open(ProgSettings["Modem_1"])
        else
            print(">> Modem not found on : ", ProgSettings["Modem_1"], ".")
        end
    end  
    if ProgSettings["Bridge"] then
        if peripheral.getType(ProgSettings["Bridge"]) == "openperipheral_bridge" then
            Glasses = peripheral.wrap(ProgSettings["Bridge"])
        else
            term.setTextColor(colors.red)
            print(">> Open Peripheral Bridge not found.")
            print(">> The program stop here.")
            print(">> Please ignore all errors messages bellow.")
        end
    end
    if ProgSettings["IOPeripheral"] then
        if peripheral.getType(ProgSettings["IOPeripheral"]) == "IOPeripheral" then
            IOPeripheral = peripheral.wrap(ProgSettings["IOPeripheral"])
        else
            print(">> IOPeripheral not found on :", ProgSettings["IOPeripheral"], ".")
        end
    end
end

function centerText(text, object)   -- Centre un texte, cette fonction bug encore.                              -- Not working yet.
    object.setText("")
    object.setText(text)
    local Multiplier = (object.getScale() * 5)
    local MidText = text:len()/2
    local PosX = (60 - (MidText * Multiplier)) + 5
    object.setX(PosX)
    object.setText(text)
    Glasses.sync()
end

function centerTitleText(text, Lenght)
    Title_Text_1.setText("")
    Title_Text_1.setText(text)
    local Multiplier = (Title_Text_1.getScale() * 5)
    local MidText = text:len()/2
    local PosY = (50 - (MidText * Multiplier)) + (Lenght*10)
    -- print(PosY)
    Title_Text_1.setY(PosY)
    Title_Text_1.setText(text)
    Glasses.sync()  
end

function PreInit()                  -- Initialisation des variables.                                            -- Variables Initialisation.
    DirInstall = shell.getRunningProgram()
    Users = Glasses.getUsers()      -- Permet de connaitre l'(es) utilisateur(s) des lunettes.
    -- URL_Date = "http://rootslayers.esy.es/date.php"
    -- URL_Time = "http://rootslayers.esy.es/Timer.php"
    
    Colors = {
        red =       0xff3333,
        blue =      0x00C7FF,
        black =     0x000000,
        yellow =    0xffff4d,
        green =     0x4dff4d,
        gray =      0x141414,
        white =     0xffffff,
    }
    
    MessageList = {}
    ErrorList   = {}
    
    count = 0
    Nb_request_Date = 0
    Nb_request_Time = 0
    MessageDisplayed = false   
end

function InitBoxes()                -- Dessine les fonds des lunettes.                                          -- Draw glasses background.
    Glasses.clear()
    --                              (xxx, yyy, width, height, color?, opacity?) 
    MainBox         = Glasses.addBox(010, 010, 001, 50, Colors.gray,  0.7)
    outLineTop      = Glasses.addBox(010, 010, 001, 01, Colors.black)
    outLineRight    = Glasses.addBox(120, 010, 001, 00, Colors.black)
    outLineLeft     = Glasses.addBox(010, 010, 001, 50, Colors.black)
    outLineBottom   = Glasses.addBox(010, 050, 001, 01, Colors.black)
    
    SplitHeader     = Glasses.addBox(010, 020, 001, 01, Colors.black)
    LineHeader      = Glasses.addBox(000, 010, 001, 00, Colors.black)
    
    SplitMessage    = Glasses.addBox(010, 060, 001, 01, Colors.black)
    
    MessageBox      = Glasses.addBox(010, 060, 110, 00, Colors.gray,  0.7)
    MoutLineRight   = Glasses.addBox(010, 060, 001, 01, Colors.black)
    MoutLineLeft    = Glasses.addBox(010, 060, 001, 01, Colors.black)
    MoutLineBottom  = Glasses.addBox(010, 060, 111, 01, Colors.black, 0.0)   
  
    Glasses.sync()
end

function InitTexts()                -- Creation des textes.                                                     -- Creation of the texts.
    -- Need rework
    Debug_Text =    Glasses.addText(200, 12, "", Colors.white)
    Error_Text =    Glasses.addText(200, 20, "", Colors.white)
    INFO_Text =     Glasses.addText(200, 5, "", Colors.yellow)
    -- Need rework
  
    TPS_Text        = Glasses.addText(12, 13, "", Colors.white)
    Date_Text       = Glasses.addText(80, 12, "", Colors.white)
    
    Clock           = Glasses.addText(30, 28, "", Colors.white)
    
    Heure_Text      = Glasses.addText(40, 28, "", Colors.white)
    Dot_Text        = Glasses.addText(65, 28, "", Colors.white)
    Minute_Text     = Glasses.addText(70, 28, "", Colors.white)
    
    Error_Hour_Text = Glasses.addText(00, 00, "", Colors.red)
    
    Message         = Glasses.addText(25, 52, "", Colors.white)
    
    Title_Text_1    = Glasses.addText(12, 150, "", Colors.blue)
    
    Message_Text_1  = Glasses.addText(25, 010 + 52, "", Colors.blue)
    Message_Text_2  = Glasses.addText(25, 020 + 52, "", Colors.blue)
    Message_Text_3  = Glasses.addText(25, 030 + 52, "", Colors.white)
    Message_Text_4  = Glasses.addText(25, 040 + 52, "", Colors.blue)
    Message_Text_5  = Glasses.addText(25, 050 + 52, "", Colors.white)
    Message_Text_6  = Glasses.addText(25, 060 + 52, "", Colors.blue)
    Message_Text_7  = Glasses.addText(25, 070 + 52, "", Colors.white)
    Message_Text_8  = Glasses.addText(25, 080 + 52, "", Colors.blue)
    Message_Text_9  = Glasses.addText(25, 090 + 52, "", Colors.white)
    Message_Text_10 = Glasses.addText(25, 100 + 52, "", Colors.blue)
    Message_Text_11 = Glasses.addText(25, 110 + 52, "", Colors.blue)
    Message_Text_12 = Glasses.addText(25, 120 + 52, "", Colors.blue)
    
    Temp_Text_1  = Glasses.addText(45, 10 + 52, "", Colors.blue)
    Temp_Text_2  = Glasses.addText(45, 20 + 52, "", Colors.white)
    Temp_Text_3  = Glasses.addText(45, 30 + 52, "", Colors.white)
    Temp_Text_4  = Glasses.addText(45, 40 + 52, "", Colors.white)
    Temp_Text_5  = Glasses.addText(45, 50 + 52, "", Colors.white)
    Temp_Text_6  = Glasses.addText(45, 60 + 52, "", Colors.white)
    
    
    INFO_Text.setScale(0.75)
    Error_Text.setScale(0.75)
    Clock.setScale(2)
    Message.setScale(1)
    Heure_Text.setScale(2)
    Minute_Text.setScale(2)
    Dot_Text.setScale(2)
    TPS_Text.setScale(1)
    Date_Text.setScale(1)
    
    Title_Text_1.setRotation(-90)
    INFO_Text.setRotation(0)
    Error_Text.setRotation(0)
    Message.setRotation(0)
    Heure_Text.setRotation(0)
    Dot_Text.setRotation(0)
    Date_Text.setRotation(0)
    TPS_Text.setRotation(0)
    Date_Text.setRotation(0)
    
    if ProgSettings["English"] then
        centerText("Hi, "..Users[1].name, Message)
    else
        centerText("Salut, "..Users[1].name, Message)
    end
    Glasses.sync()
end

function getSettings()              -- Permet de savoir le format de l'heure (12H/24H) et de la date (JJ-MM / JJ/MM)    --
    if ProgSettings["Date_Format"] == "N/D" or  ProgSettings["Time_Format"] == "N/D" or ProgSettings["getTimeOF"] == "N/D" or ProgSettings["Time_Format"] == "" or not (ProgSettings["getTimeOF"]) or not (ProgSettings["Time_Format"]) then
        Heure_Text.setText("00")
        Dot_Text.setText(":")
        Minute_Text.setText("00")
        Glasses.sync()
        
        MessageDisplayed = true
        Lenght = 3
        MoutLineRight.setX(120)
        Glasses.sync()
            
        for i=0, Lenght*10, 2 do
            MoutLineRight.setHeight(i)
            MoutLineLeft.setHeight(i)
            MessageBox.setHeight(i)
            MoutLineBottom.setY(i + 60)
            Glasses.sync()
            --sleep(0.005)
        end
            
        MoutLineBottom.setOpacity(1)
        Glasses.sync()
        
        if ProgSettings["English"] then    
            centerText("Fast settings ?", Temp_Text_1)
            centerText("1 : Yes", Temp_Text_2)
            centerText("2 : Nop", Temp_Text_3)
        else
            centerText("Config. rapide ?", Temp_Text_1)
            centerText("1 : Oui", Temp_Text_2)
            centerText("2 : Non", Temp_Text_3)
        end

        local _, __, ___, ____, msg = os.pullEvent("glasses_chat_command") 
        
        if msg == "2" then               
            if ProgSettings["English"] then    
                centerText("Date Format ?", Temp_Text_1)
                centerText("1 : MM-DD", Temp_Text_2)
                centerText("2 : MM/DD", Temp_Text_3)
            else
                centerText("Format de la date?", Temp_Text_1)
                centerText("1 : JJ-MM", Temp_Text_2)
                centerText("2 : JJ/MM", Temp_Text_3)
            end

            local _, __, ___, ____, msg = os.pullEvent("glasses_chat_command")
            if msg == "1" then
                ProgSettings["Date_Format"] = "-"
            elseif msg == "2" then
                ProgSettings["Date_Format"] = "/"
            else
                ProgSettings["Date_Format"] = "/"
            end
            
            Glasses.sync()
           
            if ProgSettings["English"] then    
                centerText("Time format?", Temp_Text_1)
                centerText("1 : 24H", Temp_Text_2)
                centerText("2 : 12H", Temp_Text_3)
                Glasses.sync()
            else
                centerText("Format de l'heure ?", Temp_Text_1)
                centerText("1 : 24H", Temp_Text_2)
                centerText("2 : 12H", Temp_Text_3)
                Glasses.sync()
            end

            local _, __, ___, ____, msg = os.pullEvent("glasses_chat_command")
            
            if msg == "1" then
                ProgSettings["Time_Format"] = "24H"
            elseif msg == "2" then
                ProgSettings["Time_Format"] = "12H"    
            else
                ProgSettings["Time_Format"] = "24H"
            end
            
            if ProgSettings["English"] then    
                centerText("IRL Time?", Temp_Text_1)
                centerText("1 : Yes", Temp_Text_2)
                centerText("2 : No", Temp_Text_3)
                Glasses.sync()
            else
                centerText("Heure IRL ?", Temp_Text_1)
                centerText("1 : Oui", Temp_Text_2)
                centerText("2 : Non", Temp_Text_3)
                Glasses.sync()
            end

            local _, __, ___, ____, msg = os.pullEvent("glasses_chat_command")
            if msg == "1" then
                ProgSettings["getTimeOF"] = "IRL"
            elseif msg == "2" then
                ProgSettings["getTimeOF"] = "IG"    
            else
                ProgSettings["getTimeOF"] = "IG"
            end
            
            if ProgSettings["getTimeOF"] == "IRL" then
                    Temp_Text_4.setText("")
                    centerText("GTM + ?", Temp_Text_1)
                    centerText("$$<numbre>", Temp_Text_2)
                    centerText("", Temp_Text_3)
                    Glasses.sync()
                local _, __, ___, ____, msg = os.pullEvent("glasses_chat_command")
                    ProgSettings["GTM"] = msg
            else
                ProgSettings["GTM"] = 0
            end

        else
            ProgSettings["Date_Format"] = "/"
            ProgSettings["GTM"] = "0"
            ProgSettings["Time_Format"] = "12H"
            ProgSettings["getTimeOF"] = "IG"
            ProgSettings["English"] = true
        end
        closeAnimation()
        Glasses.sync()
        WriteFile()
    end    
end

function deleteTextMessage()        -- Supprime les textes des INFOS.                                           -- Delete about texts.
    Title_Text_1.setText("")
    
    Message_Text_1.setText("")
    Message_Text_2.setText("")
    Message_Text_3.setText("")
    Message_Text_4.setText("")
    Message_Text_5.setText("")
    Message_Text_6.setText("")
    Message_Text_7.setText("")
    Message_Text_8.setText("")
    Message_Text_9.setText("")
    Message_Text_10.setText("")
    Message_Text_11.setText("")
    Message_Text_12.setText("")
    Glasses.sync()
end

function deleteTEMPText()           -- Supprime les textes temporaires.                                         -- Delete temp texts.
    Temp_Text_1.setText("")
    Temp_Text_2.setText("")
    Temp_Text_3.setText("")
    Temp_Text_4.setText("")
    Temp_Text_5.setText("")
    Temp_Text_6.setText("")
    Glasses.sync()
end

function deleteHeader()             -- Supprime le texte des TPS et de la date.                                 -- Delete TPS & date texts.
    TPS_Text.setText("")
    Date_Text.setText("")
    Glasses.sync()
end

function startupAnimation()         -- Animation au lancement du programme.                                     -- Animation at the program startup.         
    for i=0, 17 do
        MainBox.setWidth(i*6 + 8)
        outLineTop.setWidth(i*6 + 8)
        outLineBottom.setWidth(i*6 + 8)
        SplitHeader.setWidth(i*6 + 8)
        SplitMessage.setWidth(i*6 + 8)
        Glasses.sync()
        --sleep(0.005)
    end
    
    outLineRight.setHeight(51)
    LineHeader.setX(65)
    LineHeader.setHeight(10)
    Glasses.sync()
end

function aboutAnimation()           -- Animation des Infos.                                                     -- About Animation.
    if MessageDisplayed == false then
		Lenght = 12
        MoutLineRight.setX(120)
        for i=0, Lenght*10, 2 do
            MoutLineRight.setHeight(i)
            MoutLineLeft.setHeight(i)
            MessageBox.setHeight(i)
            MoutLineBottom.setY(i + 60)
            Glasses.sync()
            --sleep(0.0025)
        end
        
        MoutLineBottom.setOpacity(1)
        Glasses.sync()
        GUID = tostring(Glasses.getGuid())
        
        
        if ProgSettings["English"] then       
            centerTitleText("- About -", Lenght)
            centerText("Created by :", Message_Text_2)
            centerText("RootSlayers", Message_Text_3)
            centerText("Version :", Message_Text_4)
            centerText(Version, Message_Text_5)
            centerText("GUID :", Message_Text_6)
            centerText(GUID, Message_Text_7)
            centerText("CC Label :", Message_Text_8)
            centerText(os.getComputerLabel(), Message_Text_9)
            centerText("CC ID : " ..os.getComputerID(), Message_Text_10)
            centerText("", Message_Text_11)
            centerText("- $$close -", Message_Text_12)
            
        else
            centerTitleText("- Infos -", Lenght)
            centerText("Cree par :", Message_Text_2)
            centerText("RootSlayers", Message_Text_3)
            centerText("Version :", Message_Text_4)
            centerText(Version, Message_Text_5)
            centerText("GUID :", Message_Text_6)
            centerText(GUID, Message_Text_7)
            centerText("Label du PC :", Message_Text_8)
            centerText(os.getComputerLabel(), Message_Text_9)
            centerText("ID du PC : " ..os.getComputerID(), Message_Text_10)
            centerText("", Message_Text_11)
            centerText("- $$close -", Message_Text_12)
        end
        MessageDisplayed = true
        Glasses.sync()
    end
end

function helpAnimation()            -- Animation quand '$$help' est tappé.                                      -- When '$$help' is typped.
       if MessageDisplayed == false then
        MoutLineRight.setX(120)
        Lenght = 10
        
        for i=0, Lenght*10, 2 do
            MoutLineRight.setHeight(i)
            MoutLineLeft.setHeight(i)
            MessageBox.setHeight(i)
            MoutLineBottom.setY(i+ 60)
            Glasses.sync()
            --sleep(0.005)
        end
        
        MoutLineBottom.setOpacity(1)
        Glasses.sync()
        
        centerTitleText("- Help -", Lenght)
        centerText("Use '$$<command>'", Message_Text_2)
        centerText("restart", Message_Text_3)
        centerText("shutdown", Message_Text_4)
        centerText("12H/24H", Message_Text_5)
        centerText("IG/IRL", Message_Text_6)
        centerText("message", Message_Text_7)
        centerText("update", Message_Text_8)
        centerText("", Message_Text_9)
        centerText("- $$close - ", Message_Text_10)
        
        MessageDisplayed = true
    end 
end

function settingsAnimation()        -- $$Settings
    
end

function messageAnimation()         -- $$message
    if MessageDisplayed == false then
		Lenght = 12
        MoutLineRight.setX(120)
        for i=0, Lenght*10, 2 do
            MoutLineRight.setHeight(i)
            MoutLineLeft.setHeight(i)
            MessageBox.setHeight(i)
            MoutLineBottom.setY(i + 60)
            Glasses.sync()
            --sleep(0.0025)
        end
        
        MoutLineBottom.setOpacity(1)
        Glasses.sync()
        
        --
        
        MessageDisplayed = true
        Glasses.sync()
    end
end

function updateAnimation()          -- Animation quand '$$help' est tappé.                                      -- When '$$update' is typped.
       if MessageDisplayed == false then
        MoutLineRight.setX(120)
        Lenght = 7
        MessageDisplayed = true
        
        for i=0, Lenght*10, 2 do
            MoutLineRight.setHeight(i)
            MoutLineLeft.setHeight(i)
            MessageBox.setHeight(i)
            MoutLineBottom.setY(i+ 60)
            Glasses.sync()
            --sleep(0.005)
        end
        
        MoutLineBottom.setOpacity(1)
        Glasses.sync()
     
        centerText("- Update -", Message_Text_1)
        centerText("", Message_Text_2)
        centerText("Remove Settings ?", Message_Text_3)
        centerText("1 : Yes", Message_Text_4)
        centerText("2 : No", Message_Text_5)
        centerText("", Message_Text_6)
        centerText("- $$close -", Message_Text_7)
        
        local _, __, ___, ____, msg = os.pullEvent("glasses_chat_command") 
        
        if msg == "1" then
            commandUpdate(true)
        elseif msg == "2" then
            commandUpdate(false)
        else
            closeAnimation()
        end
        
        
    end 
end

function closeAnimation()           -- Ferme l'onglet message.                                                  -- Close Animation.
	if MessageDisplayed == true then
		deleteTextMessage()
		deleteTEMPText()
		MoutLineBottom.setY(60)
        Glasses.sync()
        
		for i=Lenght*10, 0, -3 do
			MoutLineRight.setHeight(i)
			MoutLineLeft.setHeight(i)
			MessageBox.setHeight(i)
            Glasses.sync()
            --sleep(0.0025)
		end
		MessageDisplayed = false
        Glasses.sync()
	end
end

function closeAll()                 -- ...                                                                      -- ...
    if MessageDisplayed then
        closeAnimation()
    end
    deleteTextMessage()
    deleteTEMPText()
    deleteHeader()
    Shutdown = true
    Heure_Text.setText("")
    Dot_Text.setText("")
    Minute_Text.setText("")
    Message.setText("")
	MoutLineBottom.setX(0)
	MoutLineBottom.setHeight(0)
    outLineRight.setHeight(0)
    LineHeader.setX(0)
    LineHeader.setHeight(0)
    
    Glasses.sync()
    
    for i=17, 0, -1 do
        MainBox.setWidth(i*6 + 8)
        outLineTop.setWidth(i*6 + 8)
        outLineBottom.setWidth(i*6 + 8)
        SplitHeader.setWidth(i*6 + 8)
        SplitMessage.setWidth(i*6 + 8)
        Glasses.sync()
        --sleep(0.0025)
    end
    Glasses.clear()
end

function getTPS()                   -- Affiche les TPS du serveur, le mod IOPeripheral est requis.              -- Display server TPS, IOPeripheral mod is requiered.
	
    -- commands.exec("TPS")
    
    if IOPeripheral then
		while 29 do
			IOPeripheral.setMeasuringTPS(true)
			TPS_u = IOPeripheral.getTPS()
			TPS = tonumber(textutils.serialize(TPS_u["averageTPS"]))
			if TPS > 30 then 
                TPS_Text.setColor(Colors.yellow)
                TPS_Text.setText(TPS.." Stop Cheat!")
            elseif (TPS > 18.99 and TPS < 29.99) then
				TPS_Text.setColor(Colors.green)
				TPS_Text.setText("TPS : "..TPS)
			elseif (TPS < 18.99 and TPS > 15) then
				TPS_Text.setColor(Colors.yellow)
				TPS_Text.setText("TPS : "..TPS)
			elseif (TPS < 15 and TPS > 10) then
				TPS_Text.setColor(Colors.red)
				TPS_Text.setText("TPS : "..TPS)
            elseif (TPS < 9.99) then
                TPS_Text.setColor(Colors.red)
                TPS_Text.setText(TPS.." Buy a PC !")
			end
			sleep(1)
		end
	else
        TPS_Text.setScale(0.75)
		TPS_Text.setColor(Colors.red)
		TPS_Text.setText("No IOPeriph.")
        table.insert(ErrorList, "No IOPeripheral")
	end
end

function getDate()                  -- ...                                                                      -- ...
    local resp, date = getURL(URL_Date)
    local i=0
    
    while (resp ~= "success") and (i < 3) do -- Tant qu'on a pas recus la reponse "success"
        i = i + 1
        Date_Text.setScale(0.75)
        Date_Text.setX(68)
        Date_Text.setColor(Colors.white)
        Glasses.sync()
        if ProgSettings["English"] then
            Date_Text.setText("Connecting.")
            Glasses.sync()
            sleep(0.2)
            Date_Text.setText("Connecting..")
            Glasses.sync()
            sleep(0.2)
            Date_Text.setText("Connecting...")
            Glasses.sync()
            sleep(0.2)
        else
            Date_Text.setText("Connexion.")
            Glasses.sync()
            sleep(0.2)
            Glasses.sync()
            Date_Text.setText("Connexion..")
            Glasses.sync()
            sleep(0.2)
            Date_Text.setText("Connexion...")
            Glasses.sync()
            sleep(0.2)
        end
    end
    
    if resp == "success" then                -- Si la reponse est "success" alors on affiche la Date.
        Day, Month = date:match("([^,]+)-([^,]+)")
        
        if ProgSettings["English"] then
            Date_Text.setText(Month..ProgSettings["Date_Format"]..Day)
            Glasses.sync()
        else
            Date_Text.setText(Day..ProgSettings["Date_Format"]..Month)
            Glasses.sync()
        end
    else                                     -- Sinon on affiche le message d'erreur.
        if Nb_request_Date == 2 then
            Void()
        end
        
        Date_Text.setScale(0.75)
        Date_Text.setX(68)
        Date_Text.setColor(Colors.red)
        Glasses.sync()
        
        if ProgSettings["English"] then
            Date_Text.setText("Error.")
        else
            Date_Text.setText("Erreur.")
        end
        
        table.insert(ErrorList, "Can't get date.")
        
        for i = 30, 1 , -1 do
            sleep(1)
        end
        
        Nb_request_Date = Nb_request_Date + 1
        
        if Nb_request_Date ~= 2 then
            getDate()
        end    
    end 
end

function getTime()                  -- ...                                                                      -- ...
    while 29 and not Shutdown do
        if ProgSettings["getTimeOF"] == "IRL" then
            Blink = true
			local resp, time = getURL(URL_Time)
            local i=0
            while (resp ~= "success") and (i < 3) do
                i = i + 1
                Clock.setScale(1)
                Clock.setX(45)
                Clock.setY(50)
                Clock.setColor(Colors.white)
                Heure_Text.setText("")
                Minute_Text.setText("")
				Clock.setText("")
                Glasses.sync()
                if ProgSettings["English"] then
                    Clock.setText("Connecting.")
                    Glasses.sync()
                    sleep(0.2)
                    Clock.setText("Connecting..")
                    Glasses.sync()
                    sleep(0.2)
                    Clock.setText("Connecting...")
                    Glasses.sync()
                    sleep(0.2)
                else
                    Clock.setText("Connexion.")
                    Glasses.sync()
                    sleep(0.2)
                    Clock.setText("Connexion..")
                    Glasses.sync()
                    sleep(0.2)
                    Clock.setText("Connexion...")
                    Glasses.sync()
                    sleep(0.2)
                end
            end
            Clock.setText("")
            if resp == "success" then
                Heure, Minute = time:match("([^,]+):([^,]+)")
                Heure = tostring(tonumber(Heure + ProgSettings["GTM"]))
                
                if Heure == "24" then
                    Heure = "0"
                elseif Heure == "25" then
                    Heure = "1"
                elseif Heure == "26" then
                    Heure = "2"
                end
                

                if ProgSettings["Time_Format"] == "12H" then   
                    Heure_Text.setX(50)
                    Clock.setScale(0.75)
                    Clock.setX(35)
                    Clock.setY(32)
					Clock.setText("PM")
                    Glasses.sync()

                    if(Heure > 12) then
                        Heure = Heure - 12
                    else
                        Clock.setText("AM")
                    end
					if #Heure == 2 then
						Heure_Text.setX(42)
					else
						Heure_Text.setX(50)
					end
                    Heure_Text.setText(Heure)
                    Minute_Text.setText(Minute)
                    Glasses.sync()
                else
					if #Heure == 2 then
						Heure_Text.setX(42)
                        Glasses.sync()
					else
						Heure_Text.setX(50)
                        Glasses.sync()
					end
					Heure_Text.setText(Heure)
                    Minute_Text.setText(Minute)
                    Glasses.sync()
                end
            else
                if Nb_request_Time == 2 then
                    Clock.setScale(0.75)
                    Clock.setX(12)
                    Heure_Text.setText("")
                    Minute_Text.setText("")
                    Clock.setColor(Colors.red)
                    if ProgSettings["English"] then
                        Message.setText("No connection.")
                    else
                        Message.setText("Aucune connexion.")
                    end
                    Glasses.sync()
                    Void()
                end
                Message.setScale(0.75)
                Message.setX(11)
                Message.setY(30)
                Message.setColor(Colors.red)
                table.insert(ErrorList, "Can't get Time")
                for i=30, 1, -1 do
                    Message.setScale(0.65)
                    Heure_Text.setText("")
                    Minute_Text.setText("")
                    if ProgSettings["English"] then
                        Message.setText("Next try in "..i.."s")
                        Message.setColor(Colors.red)
                        Message.setText("$$stop : to cancel the connection.")
                        Glasses.sync()
                    else
                        Clock.setText("Prochain essais dans "..i.."s")
                        Message.setColor(Colors.red)
                        Message.setText("$$stop : pour annuler.")
                        Glasses.sync()
                    end
                    sleep(1)
                end   
                Nb_request_Time = Nb_request_Time + 1
            end
            
            sleep(60)
        else		
			--Blink = false
			if ProgSettings["Time_Format"] == "12H" then
				time = textutils.formatTime(os.time(), false)
				--Creer une declaration text pour le text AMPM a la place du Clock.
				Heure, Minute, AMPM = time:match("([^,]+):([^,]+) ([^,]+)")
				Clock.setScale(0.75)
                Clock.setX(35)
                Clock.setY(32)
				if AMPM == "AM" then
					Clock.setText("AM")
				else
					Clock.setText("PM")
				end		

                Glasses.sync()
                
			else	
				time = textutils.formatTime(os.time(), true)
				Heure, Minute = time:match("([^,]+):([^,]+)")
			end
			if #Heure == 2 then
				Heure_Text.setX(42)
			else
				Heure_Text.setX(50)
            end
			Heure_Text.setText(Heure)
			Minute_Text.setText(Minute)
			Dot_Text.setText(":")
            Glasses.sync()
			sleep(0.83)
        end
    end    
end

function dotBlinking()              -- Clignotemant des ':'.                                                    -- ':' blinking.	
	while 29 do
		if not Shutdown then
			Dot_Text.setText(":")
			Glasses.sync()
            sleep(0.5)
			Dot_Text.setText(" ")
            Glasses.sync()
			sleep(0.5)
		else
            return
		end	
	end
end

function EventChatCommand()         -- Evennement des commandes du tchat.                                       -- Chat Command Event.
    while 29 and not Shutdown do
        local _, __, ___, ____, msg = os.pullEvent("glasses_chat_command")
        if msg == "restart" then
            os.reboot()
        elseif msg == "shutdown" then
            os.shutdown()
        elseif msg == "about" then
            if MessageDisplayed == false then
                aboutAnimation()
            else
                closeAnimation()
                aboutAnimation()
            end
        elseif msg == "close" then
            closeAnimation()
        elseif msg == "update" then 
            if MessageDisplayed == false then
                updateAnimation()
            else
                closeAnimation()
                updateAnimation()
            end   
        elseif msg == "settings" or msg == "setting" then
            if MessageDisplayed == false then
                settingsAnimation()
            else
                closeAnimation()
                settingsAnimation()
            end
        elseif msg == "message" or msg == "messages" then
            if MessageDisplayed == false then
                messageAnimation()
            else
                closeAnimation()
                messageAnimation()
            end    
        elseif msg == "help" then
            if MessageDisplayed == false then
                helpAnimation()
            else
                closeAnimation()
                helpAnimation()
            end
        elseif msg == "12H" or msg == "12h" then
            ProgSettings["Time_Format"] = "12H"
            centerText("Effective in the next 30s", Message)
            WriteFile()
            Glasses.sync()
            sleep(2)
            Message.setText("")
            Glasses.sync()
        elseif msg == "24H" or msg == "24h" then
            ProgSettings["Time_Format"] = "24H"
            centerText("Effective in the next 30s", Message)
            WriteFile()
            Glasses.sync()
            sleep(2)
            Message.setText("")
            Glasses.sync()
        elseif msg == "IG" or msg == "ig" then
            ProgSettings["getTimeOF"] = "IG"
            centerText("Effective in the next 30s", Message)   
            WriteFile()
            Glasses.sync()
            sleep(2)
            Message.setText("") 
            Glasses.sync()
		elseif msg == "IRL" or msg == "irl" then
            ProgSettings["getTimeOF"] = "IRL"
            centerText("Effective in the next 30s", Message)   
            WriteFile() 
            Glasses.sync()
            sleep(2)
            Message.setText("")
            Glasses.sync()
		elseif msg == "francais" then
			ProgSettings["English"] = false
			WriteFile()
			os.reboot()
        elseif msg == "english" then
			ProgSettings["English"] = true
			WriteFile()
			os.reboot()
		else
            if ProgSettings["English"] then
                Message.setColor(Colors.red)
                centerText("Unknow command", Message)
            else
                Message.setColor(Colors.red)
                centerText("Commande inconnue", Message)
            end
            if MessageDisplayed == false then
                helpAnimation()
            else
                closeAnimation()
                helpAnimation()
            end
            Glasses.sync()
            sleep(2)
        end
    end
end

function EventRednet()              -- Evennement des messages Rednet.                                          -- Rednet message event.
    while 29 and not Shutdown  do
        if not Shutdown then
            Message.setScale(1)
            if ProgSettings["Modem_1"] then
                if ProgSettings["English"] then
                    centerText("No new message", Message)
                else
                    centerText("Aucun message", Message)
                end    
                --Ici boucle infinie pour recevoir des msg
            else
                Message.setColor(Colors.red)
                if ProgSettings["English"] then
                    centerText("No modem", Message)
                else
                    centerText("Pas de modem", Message)
                end  
                Void()
            end
            sleep(2)
        else
            Void()
        end
    end    
end

function MainScreen()
    term.clear()
    term.setCursorPos(1, 1)
    term.setTextColor(colors.blue)
    print("Slayers' HUB is currently running.")
    term.setTextColor(colors.orange)
    print("Version : ", Version)
    term.setTextColor(colors.red)
    print("")
    term.setTextColor(colors.blue)
    print("")
    print("")
    print("")
    print("")
    term.setTextColor(colors.green)
    print("[>] Update.")
    term.setTextColor(colors.blue)
    print("[>] Settings.")
    print("")
    term.setTextColor(colors.orange)
    print("[>] Reboot.")
    term.setTextColor(colors.red)
    print("[X] Shutdown.")    
    print("[^] Run command.")    
    print("")
    print("")
    print("")
    print("")
    term.setTextColor(colors.green)
    print("Thanks for using my program ! :)")
    
    term.setTextColor(colors.red)
    while 29 and not Shutdown do
        local _, ___, x, y = os.pullEvent("mouse_click")
        
        if (x == 1) or (x==2) or (x==3) then
            if y == 8 then
                commandUpdate()
            elseif y == 9 then
                EditSettings()
            -- elseif y == 10 then
            elseif y == 11 then
                os.reboot()
            elseif y == 12 then
                os.shutdown()
            elseif y == 13 then
                shell.run("bg shell")
            end
        end
    end
end

function Init()
    Shutdown = false
    ReadFile()
    LinkPeriph()
    PreInit()
    InitBoxes()
    startupAnimation()
    InitTexts()
    getSettings()
    getDate()
end

function Debug()
    if ProgSettings["Debug"] then
        print("Debug enabled.")
        shell.run("bg shell")
    else
        return
    end
end


------------------
-- Main Program --
------------------

if not os.version() == "CraftOS 1.7" then print("ComputerCraft version is not supported. Please use CraftOS 1.7 !") return end
if not http then print("Need HTTP enabled !") return end
Init()



parallel.waitForAny(MainScreen, getTime, dotBlinking, EventChatCommand, EventRednet)



closeAll()
term.setTextColor(colors.red)
print("There should be an error somewhere. :o Sorry !")